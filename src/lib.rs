//! The `mutli_error` macro allows you to easily construct an error enum from
//! multiple pre-existing errors and efficiently use them in your code. 
//! 
//! ## Example
//! ```
//! multi_error!(DocumentError,
//!     IoError: stdP<str>
//!     EncodingError: str::str::Utf8Error
//! )

/// This macro allows you to easier make a type to simply propagate errors with
/// ease.
/// 
/// Define an error type like this:
/// ```
/// multi_error!(FileDocumentError,
///   "an error occured while read the document from file",
///   IoError: std::io::Error,
///   EncodingError: std::str::Utf8Error,
/// );
/// ```
/// And then with a return type of `Result<_, FileDocumentError>` the `?`
/// operator will automatically convert the errors into your error type for you.
/// 
/// You can put a `pub` (or another visibility modifier) before the name to
/// allow access from other modules.
#[macro_export]
macro_rules! multi_error {
  ($visibility:vis $enum_name:ident, $description:expr, $( $variant_name:ident : $variant_error:ty ),+) => {
    #[derive(Debug)]
    $visibility enum $enum_name {
      $( $variant_name($variant_error) ),*
    }

    impl std::error::Error for $enum_name {
      fn description(&self) -> &str {
        $description
      }
    }

    impl std::fmt::Display for $enum_name {
      fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        use std::error::Error;
        write!(f, "{}: ", self.description()).and(match self {
          $( $enum_name::$variant_name(e) => e.fmt(f) ),*
        })
      }
    }

    $(
      impl From<$variant_error> for $enum_name {
        fn from(error: $variant_error) -> Self {
          $enum_name::$variant_name(error)
        }
      }
    )*
  };
}


#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}

